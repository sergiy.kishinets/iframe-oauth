module OmniAuth
  module Strategies
    class Doorkeeper < OmniAuth::Strategies::OAuth2
      option :name, :doorkeeper

      option :client_options,
             site: ENV["DOORKEEPER_APP_URL"],
             authorize_path: "/oauth/authorize"

      uid do
        raw_info["id"]
      end

      info do
        {
          email: raw_info["email"]
        }
      end

      # authentication through provider is not required
      # fake data
      def raw_info
        @raw_info ||= {
          "id" => rand(10000),
          "email" => "#{rand(10000)}@bar.com"
        }
      end
    end
  end
end
